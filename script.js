let submitBtn = document.querySelector(".submit-task");
let activeBtn = document.querySelector(".active");
let completedBtn = document.querySelector(".completed");
let deleteAllBtn = document.querySelector(".delete-all");
let todoListContainer = document.querySelector(".todo-list-container");
let singleTodoArrGlobal;
let active = false;

activeBtn.addEventListener("click", () => {
  let singleTodoArr = document.querySelectorAll(".single-todo-container");
  // console.log(active)
  if (active == false) {
    for (let key = 0; key < singleTodoArr.length; key++) {
      let inputBox = singleTodoArr[key].childNodes[3];
      if (inputBox.classList.contains("strike")) {
        singleTodoArr[key].style.display = "none";
      }
    }
    active = true;
  } else {
    console.log(singleTodoArrGlobal);
    for (let key = 0; key < singleTodoArr.length; key++) {
      let inputBox = singleTodoArr[key].childNodes[3];

      if (inputBox.classList.contains("strike")) {
        singleTodoArr[key].style.display = "flex";
      }
    }
    active = false;
  }
});

completedBtn.addEventListener("click", () => {
  let singleTodoArr = document.querySelectorAll(".single-todo-container");
  if (active == false) {
    for (let key = 0; key < singleTodoArr.length; key++) {
      let inputBox = singleTodoArr[key].childNodes[3];
      if (!inputBox.classList.contains("strike")) {
        singleTodoArr[key].style.display = "none";
      }
    }
    active = true;
  } else {
    console.log(singleTodoArrGlobal);
    for (let key = 0; key < singleTodoArr.length; key++) {
      let inputBox = singleTodoArr[key].childNodes[3];

      if (!inputBox.classList.contains("strike")) {
        singleTodoArr[key].style.display = "flex";
      }
    }
    active = false;
  }
});

deleteAllBtn.addEventListener("click", () => {
  todoListContainer.innerHTML = `<h3>Tasks</h3> `;
  savetodo();
});



submitBtn.addEventListener("click", () => {
  let singleTaskText = document.getElementById("task-store");
  let titleText = document.querySelector("h2");

  if (!singleTaskText.value) {
    titleText.innerText = "Enter something before submit";
  } else {
    createElement(singleTaskText.value);
  }
});


function savetodo() {
  let inputFields = document.querySelectorAll(".task-input");
  let dataArr = [];
  for (let i = 0; i < inputFields.length; i++) {
    dataArr.push(inputFields[i].value);
  }
  console.log(dataArr);
  let todoListString = JSON.stringify(dataArr);
  localStorage.setItem("todoListText", todoListString);
}

window.addEventListener("load", () => {
  let todoListStringLC = localStorage.getItem("todoListText");
  if (todoListStringLC) {
    console.log(todoListStringLC);
    let dataArrLC = JSON.parse(todoListStringLC);
    for (let i = 0; i < dataArrLC.length; i++) {
      createElement(dataArrLC[i]);
    }
  }
});


function createElement(todoText) {
  let singleTaskText = document.getElementById("task-store");
  let titleText = document.querySelector("h2");
  titleText.innerText = "Create your task";
  let singleTodo = document.createElement("div");
  singleTodo.classList.add("single-todo-container");
  singleTodo.innerHTML = `
    <input type="checkbox" class="check">
    <input type="text" 
    value="${todoText}"
    class="task-input"
    readonly >
    <button class="edit-single-todo">Edit</button>
    <button class="delete-single-todo">Delete</button>
  `;

  todoListContainer.appendChild(singleTodo);

  let inputField = singleTodo.querySelector(".task-input");
  let editBtn = singleTodo.querySelector(".edit-single-todo");
  let deleteAllBtn = singleTodo.querySelector(".delete-single-todo");

  editBtn.addEventListener("click", () => {
    if (editBtn.innerText == "Edit") {
      inputField.removeAttribute("readonly");
      inputField.style.color = "grey";
      editBtn.innerText = "Save";
    } else if (editBtn.innerText == "Save") {
      inputField.setAttribute("readonly", "readonly");
      inputField.style.color = "black";
      editBtn.innerText = "Edit";
      savetodo()
    }
  });
  deleteAllBtn.addEventListener("click", () => {
    todoListContainer.removeChild(singleTodo);
    savetodo();
  });

  let checkBox = singleTodo.querySelector(".check");
  console.log(checkBox);
  checkBox.addEventListener("click", () => {
    // console.log(inputField)
    inputField.classList.toggle("strike");
  });

  singleTaskText.value = "";
  savetodo();
}

